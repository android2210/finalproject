package com.mek.exoticpet.database.exotic

import androidx.room.Dao
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface ExoticDao {
    @Query("SELECT * FROM Exotic ORDER BY id DESC")
    fun getAll(): Flow<List<Exotic>>

    @Query("SELECT * FROM Exotic WHERE pet_type= 'mammal' ORDER BY id DESC")
    fun getMammalType(): Flow<List<Exotic>>

    @Query("SELECT * FROM Exotic WHERE pet_type= 'reptile' ORDER BY id DESC")
    fun getReptileType(): Flow<List<Exotic>>

    @Query("SELECT * FROM Exotic WHERE pet_type= 'poultry' ORDER BY id DESC")
    fun getPoultryType(): Flow<List<Exotic>>

    @Query("SELECT * FROM Exotic WHERE id= :id")
    fun getExoticById(id: Int): Flow<Exotic>
}