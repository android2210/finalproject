package com.mek.exoticpet.database

import android.content.Context
import androidx.lifecycle.ViewModelProvider.NewInstanceFactory.Companion.instance
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mek.exoticpet.database.exotic.Exotic
import com.mek.exoticpet.database.exotic.ExoticDao

@Database(entities = [Exotic::class], version = 6)
abstract class AppDatabase: RoomDatabase() {
    abstract fun exoticDao(): ExoticDao
    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase{
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    "app_database"
                ).createFromAsset("database/name.db")
                .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }
}