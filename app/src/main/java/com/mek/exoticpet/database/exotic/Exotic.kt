package com.mek.exoticpet.database.exotic

import androidx.annotation.DrawableRes
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity
data class Exotic(
    @PrimaryKey val id: Int,
    @NotNull @ColumnInfo(name = "pet_name") val petName: String,
    @NotNull @ColumnInfo(name = "pet_data") val petData: String,
    @NotNull @DrawableRes @ColumnInfo(name = "pet_image") val pet_image: Int,
    @NotNull @ColumnInfo(name = "pet_type") val petType: String
)
