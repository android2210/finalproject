package com.mek.exoticpet

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.mek.exoticpet.adapter.ExoticAdapter
import com.mek.exoticpet.databinding.FragmentMammalListBinding
import com.mek.exoticpet.databinding.FragmentPoultryListBinding
import com.mek.exoticpet.viewmodels.ExoticViewModel
import com.mek.exoticpet.viewmodels.ExoticViewModelFactory


class PoultryListFragment : Fragment() {

    private val viewModel: ExoticViewModel by activityViewModels{
        ExoticViewModelFactory(
            (activity?.application as ExoticApplication).database.exoticDao()
        )
    }

    private var _binding: FragmentPoultryListBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPoultryListBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val poultryAdapter = ExoticAdapter{
            val action = PoultryListFragmentDirections.actionPoultryListFragmentToDataPoultryFragment(
                it.id
            )
            view.findNavController().navigate(action)
        }
        binding.recyclerView2.adapter = poultryAdapter
        viewModel.getTypePoultry.observe(this.viewLifecycleOwner){ poultryItem ->
            poultryItem.let {
                poultryAdapter.submitList(it)
            }
        }
        binding?.btnBack3?.setOnClickListener {
            val action = PoultryListFragmentDirections.actionPoultryListFragmentToMenuListFragment()
            view.findNavController().navigate(action)

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}