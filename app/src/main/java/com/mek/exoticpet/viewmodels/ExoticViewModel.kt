package com.mek.exoticpet.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import com.mek.exoticpet.database.exotic.Exotic
import com.mek.exoticpet.database.exotic.ExoticDao


class ExoticViewModel(private val exoticDao: ExoticDao): ViewModel() {
    val fullExotic: LiveData<List<Exotic>> = exoticDao.getAll().asLiveData()
    val getTypeMammal: LiveData<List<Exotic>> = exoticDao.getMammalType().asLiveData()
    val getTypeReptile: LiveData<List<Exotic>> = exoticDao.getReptileType().asLiveData()
    val getTypePoultry: LiveData<List<Exotic>> = exoticDao.getPoultryType().asLiveData()

    fun getExoticId(id: Int): LiveData<Exotic> {
        return exoticDao.getExoticById(id).asLiveData()
    }
}

class ExoticViewModelFactory(
    private val exoticDao: ExoticDao
): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ExoticViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ExoticViewModel(exoticDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}









































