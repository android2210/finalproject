package com.mek.exoticpet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.mek.exoticpet.data.dataSource
import com.mek.exoticpet.database.exotic.Exotic
import com.mek.exoticpet.databinding.FragmentDataMammalBinding
import com.mek.exoticpet.viewmodels.ExoticViewModel
import com.mek.exoticpet.viewmodels.ExoticViewModelFactory


class DataMammalFragment : Fragment() {

    private var _binding: FragmentDataMammalBinding? = null
    private val binding get() = _binding!!
    private val navigationArgs: DataMammalFragmentArgs by navArgs()
    lateinit var exotic :Exotic
    private val viewModel: ExoticViewModel by activityViewModels{
        ExoticViewModelFactory(
            (activity?.application as ExoticApplication).database.exoticDao()
        )
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentDataMammalBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.id
        viewModel.getExoticId(id).observe(this.viewLifecycleOwner) { Item ->
            exotic = Item
//            println("555555555555555     "+dataSource.exoticPet[0].pet_image)
            binding.textViewData.text = Item.petData
            binding.name.text = Item.petName
//            binding.photo = Item.pet_image
//            println("5555555555555    "+Item.pet_image)
        }
        binding?.btnBackList?.setOnClickListener {
            val action = DataMammalFragmentDirections.actionDataMammalFragmentToMammalListFragment()
            view.findNavController().navigate(action)
        }
    }


}