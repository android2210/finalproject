package com.mek.exoticpet

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.mek.exoticpet.database.exotic.Exotic
import com.mek.exoticpet.databinding.FragmentDataPoultryBinding
import com.mek.exoticpet.databinding.FragmentDataReptileBinding
import com.mek.exoticpet.viewmodels.ExoticViewModel
import com.mek.exoticpet.viewmodels.ExoticViewModelFactory

class DataPoultryFragment : Fragment() {
    private var _binding: FragmentDataPoultryBinding? = null
    private val binding get() = _binding!!
    private val navigationArgs: DataPoultryFragmentArgs by navArgs()
    lateinit var exotic : Exotic
    private val viewModel: ExoticViewModel by activityViewModels{
        ExoticViewModelFactory(
            (activity?.application as ExoticApplication).database.exoticDao()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentDataPoultryBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.id
        viewModel.getExoticId(id).observe(this.viewLifecycleOwner) { Item ->
            exotic = Item

            binding.textViewData.text = Item.petData
            binding.name.text = Item.petName
        }
        binding?.btnBackList?.setOnClickListener {
            val action = DataPoultryFragmentDirections.actionDataPoultryFragmentToPoultryListFragment()
            view.findNavController().navigate(action)

        }
    }
}