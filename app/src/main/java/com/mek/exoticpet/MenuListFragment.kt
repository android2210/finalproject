package com.mek.exoticpet

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.mek.exoticpet.databinding.FragmentMenuListBinding

class MenuListFragment : Fragment() {

    private var _binding: FragmentMenuListBinding? = null
    private val binding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMenuListBinding.inflate(inflater, container, false)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnBack?.setOnClickListener {
            val action = MenuListFragmentDirections.actionMenuListFragmentToMainMenuFragment()
            view.findNavController().navigate(action)
        }

        binding?.btnMammal?.setOnClickListener {
            var action = MenuListFragmentDirections.actionMenuListFragmentToMammalListFragment()
            view.findNavController().navigate(action)
        }

        binding?.btnReptile?.setOnClickListener {
            var action = MenuListFragmentDirections.actionMenuListFragmentToReptileListFragment()
            view.findNavController().navigate(action)
        }

        binding?.btnPoultry?.setOnClickListener {
            var action = MenuListFragmentDirections.actionMenuListFragmentToPoultryListFragment()
            view.findNavController().navigate(action)
        }
    }
}