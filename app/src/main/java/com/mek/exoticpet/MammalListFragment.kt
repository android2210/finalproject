package com.mek.exoticpet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mek.exoticpet.adapter.ExoticAdapter
import com.mek.exoticpet.database.exotic.Exotic
import com.mek.exoticpet.databinding.FragmentMammalListBinding
import com.mek.exoticpet.viewmodels.ExoticViewModel
import com.mek.exoticpet.viewmodels.ExoticViewModelFactory


class MammalListFragment : Fragment() {

    private val viewModel: ExoticViewModel by activityViewModels{
        ExoticViewModelFactory(
            (activity?.application as ExoticApplication).database.exoticDao()
        )
    }

    private var _binding: FragmentMammalListBinding? = null

    private val binding get() = _binding!!

    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMammalListBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = binding.recyclerView1
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        val mammalAdapter = ExoticAdapter {
            val action = MammalListFragmentDirections.actionMammalListFragmentToDataMammalFragment(
                it.id
            )
            view.findNavController().navigate(action)
        }
        binding.recyclerView1.adapter = mammalAdapter
        viewModel.getTypeMammal.observe(this.viewLifecycleOwner){ mammalItem ->
            mammalItem.let {
                mammalAdapter.submitList(it)
            }
        }
        binding?.btnBack1?.setOnClickListener {
            val action = MammalListFragmentDirections.actionMammalListFragmentToMenuListFragment()
            view.findNavController().navigate(action)

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}