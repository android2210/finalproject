package com.mek.exoticpet

import android.app.Application
import com.mek.exoticpet.database.AppDatabase

class ExoticApplication: Application(){
    val database: AppDatabase by lazy {
        AppDatabase.getDatabase(this)
    }
}
