package com.mek.exoticpet

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.mek.exoticpet.adapter.ExoticAdapter
import com.mek.exoticpet.databinding.FragmentMammalListBinding
import com.mek.exoticpet.databinding.FragmentReptileListBinding
import com.mek.exoticpet.viewmodels.ExoticViewModel
import com.mek.exoticpet.viewmodels.ExoticViewModelFactory

class ReptileListFragment : Fragment() {
    private val viewModel: ExoticViewModel by activityViewModels{
        ExoticViewModelFactory(
            (activity?.application as ExoticApplication).database.exoticDao()
        )
    }

    private var _binding: FragmentReptileListBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentReptileListBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val reptileAdapter = ExoticAdapter{
            val action = ReptileListFragmentDirections.actionReptileListFragmentToDataReptileFragment(
                it.id
            )
            view.findNavController().navigate(action)
        }
        binding.recyclerView3.adapter = reptileAdapter
        viewModel.getTypeReptile.observe(this.viewLifecycleOwner){ reptileItem ->
            reptileItem.let {
                reptileAdapter.submitList(it)
            }
        }
        binding?.btnBack2?.setOnClickListener {
            val action = ReptileListFragmentDirections.actionReptileListFragmentToMenuListFragment()
            view.findNavController().navigate(action)

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}