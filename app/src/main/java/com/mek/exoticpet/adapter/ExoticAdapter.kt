package com.mek.exoticpet.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mek.exoticpet.R
import com.mek.exoticpet.database.exotic.Exotic
import com.mek.exoticpet.databinding.ListItemBinding


class ExoticAdapter(private val onItemClicked: (Exotic) -> Unit) : ListAdapter<Exotic, ExoticAdapter.ExoticViewHolder>(DiffCallback) {
    companion object {
        private val DiffCallback = object: DiffUtil.ItemCallback<Exotic>(){
            override fun areItemsTheSame(oldItem: Exotic, newItem: Exotic): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Exotic, newItem: Exotic): Boolean {
                return oldItem == newItem
            }

        }
    }

    class ExoticViewHolder(private var binding: ListItemBinding): RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SimpleFormat")
        fun bind(exotic: Exotic) {
            binding.nameTextView.text = exotic.petName
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExoticViewHolder {
        return ExoticViewHolder(
            ListItemBinding.inflate(
                LayoutInflater.from(
                    parent.context
                )
            )
        )
    }

    override fun onBindViewHolder(holder: ExoticViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener {
            onItemClicked(current)

        }

    }
}